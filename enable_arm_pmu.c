
#include <linux/kernel.h>
#include <linux/module.h>

void
enable_cpu_counters(void* data)
{
	asm volatile("msr pmuserenr_el0, %0" :: "r" (0x0f));
	asm volatile("msr pmcr_el0, %0" :: "r" (1));
	asm volatile("msr pmcntenset_el0, %0" :: "r" (1 << 31));
}

int init_module(void)
{
	on_each_cpu(enable_cpu_counters, NULL, 0);
	printk(KERN_INFO "CCR enabled");
	return 0;
}

void cleanup_module(void) {
}
